
docker rm -f art-webapp
docker run --restart unless-stopped -v $(pwd)/../config/certs:/usr/local/certs --tty --detach -p 8000:80 -p 443:443 --name art-webapp --network art --ip 178.178.0.20 art-webapp:latest