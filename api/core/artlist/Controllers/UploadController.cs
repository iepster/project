﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace artlist.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UploadController : ControllerBase
    {

        private readonly ILogger<UploadController> _logger;

        public UploadController(ILogger<UploadController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Movies> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 1).Select(index => new Movies
            {
                UploadedAt = DateTime.Now.AddDays(index)
            })
            .ToArray();
        }
    }
}
