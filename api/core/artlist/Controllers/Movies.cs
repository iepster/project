﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace artlist.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MoviesController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "artlist.mp4", "iepster.mp4", "outdoor.mp4", "cool.mp4"
        };

        private readonly ILogger<MoviesController> _logger;

        public MoviesController(ILogger<MoviesController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Movies> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new Movies
            {
                UploadedAt = DateTime.Now.AddDays(index),
                Hash = "59bc3789-9f5b-4ae5-818d-9b83daa5fd1c",
                Name = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}
