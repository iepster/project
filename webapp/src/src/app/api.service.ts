import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http'
import { Router } from "@angular/router"
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  public apiPath: string = '';

  constructor(private http: HttpClient, private router: Router) { 
    if(!environment.production) {
      this.apiPath = '/api';
    } 
  }

  getHeaders() {
    let headers = new HttpHeaders();
    headers = headers.append('Token', 'auth_token_for_security');

    return {headers};
  }

  uploadMovie(data) {
    return this.http.post( (environment.production ? document.location.protocol + '//' : '') + (environment.apiUrl || (environment.production ? document.location.hostname : ''))   + (environment.apiPort ? ':' + environment.apiPort : '') +  this.apiPath + '/upload',  data, this.getHeaders());
  }

  getMovies() {
    return this.http.get( (environment.production ? document.location.protocol + '//' : '') + (environment.apiUrl || (environment.production ? document.location.hostname : ''))   + (environment.apiPort ? ':' + environment.apiPort : '') +  this.apiPath + '/upload');
  }

}
