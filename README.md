Before anything, run ./deply.sh  to create the docker subnet (178.178.0.0/16)


WEBAPP:  
http://178.178.0.1/:8000  
http://178.178.0.1/  
  
API:  
http://http://178.178.0.1/:8888  
Api paths:   
/movies - get all thumbsa as base64, movie name and uploaded time  
/upload - upload the movies - validation server side  
*https missing   
  
DB:  
http://178.178.0.1/:3306  
user: root  
pass: 3wvSM78RZmdj5Arsuvc9a8Zt  


**Project structure**

Each folder contains the following structure  

/build.sh - build the docker image  
/run.sh - run the docker image  
/src - the source code of the specific container  
/core - the .NET content used to build the specific image   

**What's inside?**
Docker structure for images and containers that can be used standalone or the images with kube, etc.  
WEBAPP - angular 9 with angular material and bootstrap, basic calls validation and error handling, drag && drop file upload, grid gallery (uploaded movies) listing  
API - routes with placeholder JSON set  
DB - bulk


**To Do:**  
*Update deploy.sh to cover the whole project*  

WEBAPP:   
misc fixes on API wire
API:     
movie converter and thumb crop - FFMPEG, the only "free" lib without support for a lot of time  
update response JSONs and cors 
DB:   
Update DB structure on container up.  