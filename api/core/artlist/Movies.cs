using System;

namespace artlist
{
    public class Movies
    {
        public DateTime UploadedAt { get; set; }
        public string Name { get; set; }
        public string Hash { get; set; }
    }

    public class Response
    {
        public string Status { get; set; }
        public string Message { get; set; }
    }
}
